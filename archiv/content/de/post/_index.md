---
title: "Artikel"

description: "Data Literacy Alliance Knowledge-base for FAIR data usage and supply"
cascade:
  featured_image: 'bg_dark_3.jpg'

---
Mit dem Motto „Datenkompetenz von Anfang an!“ startet DALIA die Entwicklung eines zentralen Einstiegspunkts für eine föderierte Wissensbasis für Lehr- und Lernmaterialien zur Förderung von Datenkompetenz im Bereich der FAIRe-Datennutzung und -bereitstellung.
