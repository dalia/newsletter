---
date: "2024-03-02"
description: ""
header_image: "/Dalia_Header_Dark-Blue_RGB_3a_Kopie.jpg"
featured_image: "/Gruppenfoto2.jpg"
tags: ["newsletter"]
title: "No. 2, März 2024"
---

# DALIA nach dem ersten Projektjahr

![Gruppenfoto](/newsletter/Gruppenfoto_3.jpg)

DALIA steht für [Data Literacy Alliance](https://dalia.education/). Nach dem ersten Jahr des Projekts hat sich DALIA erneut an der Technischen
Universität Darmstadt getroffen, dieses Mal organisiert vom Team der Universitäts- und Staatsbibliothek. Das DALIA-Team bespricht die Ergebnisse des ersten Projektjahrs und legt die nächsten Schritte fest.

An der nächsten NFDI Sektionsmitgliederversammlung am 16. Mai 2024 von 13:00 bis 15:00 Uhr plant DALIA die Vorstellung des DALIA Prototypen in einer ersten Version zusammen mit einer Empfehlung für die NFDI-Community für das Bereitstellen, Zusammenführen und Sichtbarmachen von Lehr- und Lernmaterialien in dem Portal. DALIA kündigt in diesem Zusammenhang einen Aufruf zur Befüllung des Portal mit Testdaten (Lehr- und Lernmaterialien) an. 

Durch die Verbundenheit der Projektpartner mit dem [NFDI e.V.](https://www.nfdi.de/) und insbesondere der [Sektion Training & Education](https://www.nfdi.de/section-edutrain/) wird sichergestellt, dass DALIA nachhaltig in die Nationale Forschungsdateninfrastruktur (NFDI) Community integriert und eingebettet wird. Mit Blick auf eine Verstetigung und um die DALIA Plattform nachhaltig zu sichern, wird zusammen mit der [NFDI Sektion Training und Education](https://www.nfdi.de/section-edutrain/) ein [Base4NFDI](https://base4nfdi.de/) Antrag vorbereitet. 

# DALIA und die NFDI Sektion EduTrain

Die mit dem DALIA-Projekt eng verzahnten Aktivitäten der [NFDI Sektion Training & Education](https://www.nfdi.de/section-edutrain/) bekommen ab sofort einen eigenen Platz im DALIA Newsletter, so dass sie auch in diesem Rahmen kommuniziert werden können. Zum Auftakt wird kurz skizziert was in den einzelnen Arbeitspaketen derzeit passiert: In den Arbeitspaketen 1 (Zielgruppen- und Bedarfsanalyse), 3 (Entwicklung eines modularen und skalierbaren Konzeptes) und 8 (Error Culture) werden jeweils Whitepaper geschrieben. Das Whitepaper zu [EduBricks: Ein Konzept für modulares und skalierbares Lehrmaterial](https://docs.google.com/document/d/1XpHsC6HmEXGLpGOrdwu-szg5WTkb8-Q9NQ47cr14dhY/edit#heading=h.u6ni3dwuzt3h) ist bereits zur Kommentierung für die EduTrain-Community zugänglich. Ferner überarbeitet ein Editorial-Team auf Hochtouren die [Lernzielmatrix zum Forschungsdatenmanagement](https://zenodo.org/records/7034478). Kontinuierlich werden aktuelle Informationen aus der Sektion EduTrain, so wie alle weiteren Ereignisse über diverse NFDI-Kanäle, wie die [Sektions-Mailingliste](https://lists.nfdi.de/postorius/lists/section-edutrain.lists.nfdi.de/) und [NFDI Sektion RocketChat](https://all-chat.nfdi.de/channel/Section-TrainingEducation) Kanäle kommuniziert.

### Anstehende Termine:

- Jeden 1. Montag im Monat von 11:30 - 12:30 Uhr trifft sich das AP 2 - Bestandsaufnahme Lehr- und Lernmaterialien der NFDI Sektion EduTrain, online in diesem [Zoomraum](https://adwmainz.zoom.us/j/92476322440).
- Am 20. März 2024 von 14:00 - 15:00 Uhr trifft sich das - AP 5 Schulungsformate und Zertifikatskurs der NFDI Sektion EduTrain, online in diesem [Zoomraum](https://us02web.zoom.us/j/89267516658?pwd=M3Fvai83KzRMZzE1VHBYN01SVXRJdz09).
- Am 16. Mai 2024 von 13-15 Uhr findet die nächste NFDI Sektion EduTrain Mitgliederversammlung statt.
- Am 18. Juni 2024 von 11-13 Uhr organisiert AP 1 - Zielgruppen- und Bedarfsanalyse der NFDI Sektion EduTrain, einen weiteren Workshop zur Entwicklung von Personas in AP 1/DALIA. In diesem Workshop soll auf die Spezifizierung der Bedarfe von (Material nachnutzenden) Lehrenden und Inhalts-/Materialbeitragenden in DALIA eingegangen. Die Ergebnisse des Workshops werden genutzt, um die bereits skizzierten Persona-Beschreibungen zu schärfen.


# Community Event zur Lernzielmatrix zum Forschungsdatenmanagement 

![Gruppenfoto](/newsletter/Gruppenfoto2.jpg)

Vom 31. Januar bis 1. Februar 2024 fanden sich 57 Interessierte Mitglieder der Forschungsdatenmanagement-Community unter anderem aus der NFDI, von den Datenkompetenzzentren und den Landesinitiativen im Georg-Christoph-Lichtenberg-Haus in Darmstadt zusammen. Auf dem von der [Data Literacy Alliance](https://dalia.education), der  [NFDI Sektion Training und Education](https://www.nfdi.de/section-edutrain/) und der [UAG Schulungen/Fortbildungen der DINI/nestor-AG Forschungsdaten](https://www.forschungsdaten.org/index.php/UAG_Schulungen/Fortbildungen) organisierten Community-Event wurde gemeinsam der aktuellen Stand der [Lernzielmatrix zum Forschungsdatenmanagement](https://zenodo.org/doi/10.5281/zenodo.7034477 ) diskutiert, überarbeitet und weiterentwickelt, um diese künftig als Grundlage für die Kompetenzförderung im Bereich Forschungsdatenmanagement zu etablieren. 

In den nächsten Wochen darf sich die Community nicht nur auf eine neue Version der Lernzielmatrix freuen, sondern auch auf ein Linked Open Data Modell der selbigen.


# DALIA Tech Watch 

Die Vorträge dieser Reihe sind im [DALIA YouTube Kanal](https://www.youtube.com/channel/UC6f7Bm8AGdfWuAvSAwe2Bjg) verfügbar.

### 20. September 2023, "LiaScript" mit Sebastian Zug und André Dietrich (TU Freiberg)
Am 20. September 2023 haben Sebastian Zug und André Dietrich Konzepte, konkrete Umsetzungen und Erfahrungen aus dem langjährigen Einsatz von LiaScript vorgestellt.

{{< details "weiter lesen" "Mit LiaScript stellten Sebastian Zug und André Dietrich das auf Markdown basierende Werkzeug, zur Erstellung interaktiver, offener und interoperabler Bildungsinhalte vor. Durch die Erweiterung von Markdown um Funktionen wie Multimedia, Quiz, Animationen und interaktive Programmierung können vielfältige Lernmaterialien erstellt werden auch ohne umfassende Programmierkenntnisse. Diese können in verschiedene Formate exportiert und in verschiedene Lernmanagementsysteme importiert werden. Darüber hinaus können die erstellten Inhalte als Markdown-Dokumente in Git-Repositorien geteilt werden, um ihre Lesbarkeit und Bearbeitbarkeit zu gewährleisten und eine einfache Nachnutzung zu ermöglichen. Eine Anleitung für LiaScript gibt es unter anderem im eigenen [YouTube Channel](https://www.youtube.com/channel/UCyiTe2GkW_u05HSdvUblGYg)." >}}


### 24. Oktober 2023, “Data Literacy Kompetenzprofil für historisch arbeitende Disziplinen” mit Marina Lemaire und Laura Dönig (Uni Trier, eScience)
Am 24. Oktober gaben Laura Dönig und Marina Lemaire Einblicke in das Arbeiten und Mappen von Datenkompetenzmatrizen mit FuD-Arbeitsumgebung. 

{{< details "weiter lesen" "Im Rahmen des NFDI4Memory Konsortiums beschäftigt sich die Task Area 4 Data Literacy mit der Entwicklung von Services, Handreichungen und innovativen Lehr- und Lernformaten, um die Datenkompetenz (DL) und das aktive Forschungsdatenmanagement (FDM) in den historisch arbeitenden Disziplinen zu verbessern. Die Entwicklung eines historisch geprägten DL-Kompetenzprofils ist die Grundlage für ein DL-Modell-Curriculum sowie epochen- und area-spezifische Lehrpläne. Des Weiteren wird es für die Klassifizierung der Angebote des DL-Trainingskatalogs eingesetzt werden. Ihren Ausgangspunkt nahmen die Arbeiten bereits bei der Planung und Durchführung der Data Literacy Bedarfserhebung, für die ein Fragebogendesign entwickelt werden musste. Hierfür wurde unter zu Hilfenahme der [Forschungssoftware FuD](https://fud.uni-trier.de/) ein Mapping von Kompetenzmatrizen durchgeführt. In mehreren Mapping-Iterationen wurden die einzelnen Kompetenzprofile aufeinander abgebildet, sodass nach jeder Mappingphase ein erweitertes Profil entstanden, das wiederum die Ausgangsbasis für das nachfolgende Mapping wurde. Ziel des Abgleichs war es eine fusionierte, für die Geschichtswissenschaften spezialisierte Kompetenzmatrix aus den einzelnen DL-Kompetenzprofilen zu generieren." >}}


# DALIA OER Community Workshops

Mit dieser Workshopreihe wird eine Plattform zum aktiven Austausch und die projektübergreifende Kollaboration gefördert und verstetigt. Eine kontinuierliche Kommunikation wird als Schlüsselfaktor für den Projekterfolg gesehen, daher sollen alle künftigen Nutzerkreise aktiv an der Entwicklung teilhaben und die bestehenden Communitys of Practice weiter ausgebaut werden. Die Workshopergebnisse werden nachgelagert auf der Webseite veröffentlicht.

{{< details "weiter lesen: 10. Oktober 2023, online, Schwerpunkt EduTrain Personas" "Die verschiedenen Konsortien innerhalb der NFDI haben im Bereich Training & Education (EduTrain) beeindruckende Fortschritte gemacht, um die Bedürfnisse ihrer Zielgruppen zu erfüllen, haben sie entweder Personas formuliert oder disziplinspezifische Anwendungsfälle erhoben. Diesen Ansatz hat DALIA zusammen mit Britta Petersen HutträgerIn des AP1 der Sektion EduTrain verfolgt und im Rahmen eines Side-Events auf der [CoRDI 2023](https://www.nfdi.de/cordi-2023/) aufgegriffen und im Rahmen des Community Events am 10. Oktober 2023 weiter ausgebaut. Der Blick auf die NFDI verdeutlicht, dass die Konsortien die Bedürfnisse ihrer Zielgruppen mit drei grundlegenden Persona-Konzepte, Content Provider, Trainer, und Learner adressieren. Diese werden von DALIA nun übernommen und weiter charakterisiert. Für jede dieser Zielgruppen werden künftig AnsprechpartnerInnen in der Sektion Training und Education zur Verfügung stehen, die die konkreten Bedarfe weiter schärfen und in das Projekt DALIA übertragen. Wir freuen uns darauf, diese Entwicklungen weiter zu verfolgen zusammen mit der bestehenden Vielfalt der Trainingsangebote innerhalb der NFDI maßgeschneidert im DALIA Portal anzubieten. Einen ausführlicher Bericht zum Workshop ist derzeit als Whitepaper in Arbeit." >}}

{{< details "weiter lesen: 09. November 2023, online, Schwerpunkt Metadatenharmonisierung" "Die Harmonisierung von Metadaten ist ein wichtiger Teil des DALIA-Projekts für die wissenschaftliche Gemeinschaft. Dabei geht es um die Vereinheitlichung von Begriffen und deren Definitionen sowie um die Verbindung von Metadatenklassen verschiedener Standards. In diesem Community-Workshop kamen daher  Wissenschaftler und Wissenschaftlerinnen von DALIA, NFDI-Konsortien, der DINI AG KIM, dem SODa (Center for Collections, Objects, Data Competencies), dem Trier Center of Digital Humanities und anderen Institutionen zusammen um den aktuellen Stand zu erörtern und diskutieren. Den Einstieg in den Themenbereich bildeten verschiedene Vorträge: Petra Steiner (DALIA) gab eine Einführung in die minimalen Metadatensets von DALIA und deren Beschreibung in Form von Tabular Application Profiles (TAPs), die nicht nur ein effizientes und transparentes Mittel zur Beschreibung von Metadatenstandards darstellen, sondern auch zur formalen Validierung von RDF-Graphen dienen. Katharina Bergmann und Andrea Polywka von [NFDI4Culture](https://nfdi4culture.de/) stellten die Terminologie und den [Educational Resource Finder](https://nfdi4culture.de/services/details/educational-resource-finder.html) des Projekts vor. Susanne Arndt von der [NFDI AG Cookbooks, Guidance, and Best Practices](https://zenodo.org/records/6758256) gab einen Überblick über die Metadatenkategorien und Materialsammlungen der AG. Mark Fichtner präsentierte die im Datenkompetenzzentrum vorgesehenen drei Wege der anwendungsorientierte Vermittlungsmethoden für die fachübergreifende Kompetenzförderung: Onboarding, Analog und Digital. Bei den Schulungsmaterialien zu Metadaten und Terminologien gibt es große Überschneidungen mit dem Themenbereich von DALIA. Nach der Zusammenstellung dieser allgemeinen Informationen diskutierten drei parallele Gruppen ihre gemeinsamen Ideen zu minimalen Metadatensets für Lehr- und Lernressourcen. Dabei ergaben sich zahlreiche Metadatenkategorien, aber auch Definitionsfragen. Dieser Workshop initiierte einige weitere Treffen und hatte großen Einfluss auf die Entwicklung des minimalen Metadatensets von DALIA." >}}

{{< details "weiter lesen: 28. November 2023, online, Schwerpunkt DALIA Frontend" "Im letzten Workshop des Jahres stellte das DALIA-Team der FDM-Community die ersten Ergebnisse der DALIA-Portalentwicklung vor und diskutierte gemeinsam mit Teilnehmenden das Frontend der in Entwicklung befindlichen Bildungsplattform. Am 28. November 2023 lud das DALIA-Projektteam dazu ein, in einem dreistündigen Workshop das Interface des DALIA-Portals gemeinsam zu testen und zu bewerten. In enger Zusammenarbeit mit der Designagentur [Blue World Studios](https://www.blueworld.studio/) entwickelte die Projektgruppe in den vergangenen zwei Monaten die Startseite inkl. Keyword-Suchfunktion, die Ergebnisansicht für Ressourcen sowie Profil- und Community-Seiten, die nun das Feedback der Community erforderten. In gemeinsamen Diskussion zu den einzelnen Frames der entstehenden Website wurden neue Ideen und Perspektiven eingebracht, die dem Team ein tieferes Verständnis darüber brachten, wie die Funktionen und das Layout der Oberfläche für zukünftige Nutzenden optimiert werden kann. Das Feedback wurde anschließend im Team sorgfältig evaluiert und ist in die Weiterentwicklung eingeflossen. Wir möchten uns bei allen Teilnehmenden bedanken, die sich beim Workshop beteiligt haben und deren Enthusiasmus und Ideen die Fertigstellung der Plattform vorantreiben und sie zu einem besseren Ort für das Lehren und Lernen machen. Wir freuen uns darauf, das Ergebnis unserer Arbeit in diesem Jahr mit der Community teilen zu können." >}}

# DALIA upcoming events

### DALIA Tech Watch   
- 13. März 2024, online, 10:00 - 11:00 Uhr, [Open Research Knowledge Graph (ORKG)](https://www.youtube.com/watch?v=gDNt3C57OO0) mit Vinodh Ilangovan
- 07. Mai 2024 , online, 13:00 - 14:00 Uhr, “Von Daten, Kompetenzen und der Toolbox Datenkompetenz - ein Überblick” mit Christian Zinke-Wehlmann (InfAI)
- 12. Juni 2024, online, 9:30 - 10:30 Uhr, "Navigating Open Horizons: Strategies for Fostering OER competences and activities in Austrian higher education" mit Sandra Schön (TU Graz)
- 03. Juli 2024, online, 13:00 - 14:00 Uhr, "Educational Ressource Finder (ERF)" mit Andrea Polywka, Katharina Bergmann und Linnaea Söhn (NFDI4Culture)

### SAVE THE DATE | Other Events and Workshops

- 16. Mai 2024, online, NFDI Sektion EduTrain Mitgliederversammlung
- 21.-23. Oktober 2024, Berlin, [EOSC Symposium](https://eosc.eu/events/eosc-symposium-2024/)


# DALIA wird gefördert von

<a href="https://foerderportal.bund.de/foekat/jsp/SucheAction.do?actionMode=view&amp;fkz=16DWWQP07A"><img alt="Logo für Bundesministerium für Bildung und Forschung" src="/newsletter/Foerderhinweis-BMBF.jpg" width="134" height="100" /></a>
<img alt="Logo: &quot;Finanziert von der Europäischen Union - NextGenerationEU" src="/newsletter/Foerderhinweis-EU-vertikal.jpg" width="95" height="100" />


