---
date: "2023-07-01"
description: ""
header_image: "/Dalia_Header_Dark-Blue_RGB_3a_Kopie.jpg"
featured_image: "/DALIA2.jpg"
tags: ["newsletter"]
title: "No. 1, Juli 2023"
---

**Wir sind umgezogen**
, der Newsletter ist nun integriert in unsere Projektwebseite, bitte klicken sie auf den folgenden Link zur 
[[Weiterleitung]](https://dalia.education/de/newsletter/20230701_no-1-juli-2023)

# DALIA stellt sich vor


![Foto des DALIA Teams](/newsletter/DALIA2.jpg)

Vom 24.-25. März 2023 traf sich das DALIA Team zum Auftakt in Darmstadt. Dem Paradigma der FAIRen Datennutzung und Datenbereitstellung folgend wurde gemeinsam die DALIA Vision erarbeitet, „Datenkompetenz von Anfang an„ zu fördern.

[[Weitere Informationen]](https://www.fst.tu-darmstadt.de/fachgebiet/news_details_522688.de.jsp)

# Der DALIA Verbund

DALIA steht für Data Literacy Alliance. Durch die Verbundenheit der Projektpartner mit dem [NFDI e.V.](https://www.nfdi.de/), den [Sektionen Training & Education](https://www.nfdi.de/section-edutrain/), [ELSA - Ethical, Legal & Social Aspects](https://www.nfdi.de/section-elsa/) und [(Meta)daten, Terminologien, Provenienz](https://www.nfdi.de/section-metadata/) wird sichergestellt, dass DALIA nachhaltig in die Nationale Forschungsdateninfrastruktur (NFDI) Community integriert und eingebettet wird.

# Die DALIA Technologie

### DALIA, was bist Du?

DALIA ist eine Wissensbasis.

In DALIA entwickelt das Team der [Forschungsdaten-Services](https://www.ulb.tu-darmstadt.de/die_bibliothek/ueberuns/organisation/abteilung_iii/forschungsdaten_services.en.jsp) an der Universitäts- und Landesbibliothek Darmstadt (TU Darmstadt) einen ontologie-basierten Wissensgraph als Basis für die effiziente Integration und Vernetzung spezialisierter verteilter Datenquellen zur Förderung von Datenkompetenz. Die Vorzüge logischer Schlussfolgerungen werden zur Unterstützung der Kuratierung der Lernmaterialien und EduBricks als Kurspfade sowie zur Anreicherung des Empfehlungssystems eingesetzt.

# DALIA Tech Watch 

Alle Vorträge dieser Reihe sind im [DALIA YouTube Kanal](https://www.youtube.com/channel/UC6f7Bm8AGdfWuAvSAwe2Bjg) verfügbar.

### 19. Februar 2023, eDoer Prototyp-Workshop mit Gábor Kishmihók
Am 19. Februar 2023 haben unsere Projektpartner der TIB Hannover ihren Prototypen der offenen, KI-gestützten Lernplattform eDoer vorgestellt. Darüber hinaus wurden die Projekte eDoer Knowledge Graph and EduCOR ontology eingeführt. 

{{< details "weiter lesen" "eDoer ist ein Empfehlungssystem für offen zugängliche Bildungsinhalte. Die Plattform bietet Zugang zu einem breiten Spektrum an offenen Bildungsressourcen und nutzt Algorithmen des maschinellen Lernens, um sowohl Lernende, als auch Lehrende bei der Suche nach passenden, personalisierten Bildungsinhalten zu unterstützen. Mit Hilfe KI-gestützter Algorithmen werden die Lerndaten der Nutzer*innen in Bezug auf Lernpräferenzen und -ziele optimal genutzt, um die Inhalte so anzupassen, dass sie den Bedürfnissen jedes einzelnen Lernenden am besten entsprechen. Für weitere Informationen zu eDoer besuchen Sie bitte die [Webseite](https://www.edoer.eu)." >}}


### 06. Juni 2023, CESSDA Data Catalogue mit Maja Dolinar
In dieser DALIA Tech Watch-Session stellte Maja Dolinar (Universität Ljubljana) die neueste Version des CESSDA Data Catalogue (CDC) vor. CESSDA vereint sozialwissenschaftliche Datenarchive aus ganz Europa, mit dem Ziel, die Ergebnisse sozialwissenschaftlicher Forschung zu fördern und die nationale Forschung und internationale Zusammenarbeit zu unterstützen. 

{{< details "weiter lesen" "Der CESSDA Datenkatalog (CDC) ist eine zentrale Anlaufstelle für die Recherche nach sozialwissenschaftlicher Daten in Europa. Dabei kann es sich um quantitative, qualitative oder gemischte Daten handeln, um Quer- oder Längsschnittdaten, um kürzlich erhobene oder historische Daten. CESSDA DC enthält Beschreibungen aus über 40.000 Datensammlungen (> 20 europäische Länder), die von den CESSDA-Dienstanbietern (SPs) verwaltet werden. Für mehr Informationen zum CESSDA Datenkatalog, besuchen Sie [Webseite](https://www.cessda.eu/Tools/Data-Catalogue)." >}}

### 20. Juli 2023, NFDICulture Knowledge Graph mit Etienne Posthumus
Am 20. Juni 2023 gab Etienne Posthumus (FIZ Karlsruhe) eine Einführung in Knowledge-Graph (KG) basierte FDM-Lösungen und stellte anschließend den [Culture Knowledge Graph](https://nfdi4culture.de/services/details/culture-knowledge-graph.html) und seine Anwendung vor. 

{{< details "weiter lesen" "Der Culture Knowledge Graph zielt darauf ab, eine Verbindung zwischen allen in NFDI4Culture Fachgebieten erzeugten Forschungsdaten herzustellen. Dadurch soll die Auffindbarkeit, Zugänglichkeit, Interoperabilität und Wiederverwendbarkeit von Daten innerhalb der NFDI4Culture verbessert werden. Weitere Informationen zum Culture KG und zu NFDI4Culture finden Sie auf der [Webseite](https://nfdi4culture.de/de/dienste/details/culture-knowledge-graph.html)." >}}

# DALIA OER Community Workshops

Mit dieser Workshopreihe wird eine Plattform zum aktiven Austausch und die projektübergreifende Kollaboration gefördert und verstetigt. Eine kontinuierliche Kommunikation wird als Schlüsselfaktor für den Projekterfolg gesehen, daher sollen alle künftigen Nutzerkreise aktiv an der Entwicklung teilhaben und die bestehenden Communitys of Practice weiter ausgebaut werden. Die Workshopergebnisse werden nachgelagert auf der Webseite veröffentlicht.

{{< details "weiter lesen: 6. Juni 2023, online, Schwerpunkt Lehre und Didaktik" "In der ersten moderierten Fokusgruppendiskussion diskutierten Experten unter der Leitung von Sonja Herres-Pawlis, Britta Petersen, Constanze Hahn und Petra Steiner über Anforderungen und Konzepte in DALIA zu Lehre, Didaktik, Personas, Nutzerrollen und Metadaten. Die Diskussion umfasste verschiedene Aspekte der Organisation und Integration von Lehr- und Lernmaterialien. Die Teilnehmer betonten die Bedeutung von Qualität, Wiederverwendbarkeit und Vertrauen in die DALIA-Plattform, ähnlich wie bei bestehenden Angeboten wie [Carpentries](https://carpentries.org/index.html), [Elixir Tess](https://tess.elixir-europe.org/) und [dariahTeach](https://teach.dariah.eu/). Notwendige Qualitätskriterien wie fachliche, rechtliche und didaktische Aspekte wurden genannt, um die Nutzer adäquat anzusprechen und vielfältige Lernwege für individuelle Szenarien anzubieten. In diesem Zusammenhang wurden die Nutzerrollen der potentiellen DALIA-Zielgruppen mit Blick auf Personas, z.B. [Bioschemas TrainingMaterial Profile](https://bioschemas.org/profiles/TrainingMaterial/1.0-RELEASE), diskutiert. Zu diesem Thema wird es einen weiteren vertiefenden Workshop geben. Weiterhin wurde ein Ausblick auf den Umgang mit Metadaten gegeben. Neben der Nutzung bestehender Standards sollen auch bereits in der Community etablierten Ansätze wie die [Lernzielmatrix zum Themenbereich Forschungsdatenmanagement für die Zielgruppen Studierende, PhDs und Data Stewards](https://zenodo.org/record/7034478) berücksichtigt werden. Darüber hinaus hat das DALIA-Team beschlossen, ein Glossar zu führen, um einheitliche Definitionen und ein gemeinsames Verständnis von Begriffen wie Persona, User Story, Use Case und anderen zu gewährleisten." >}}

{{< details "weiter lesen: 16. Juni 2023, online, Schwerpunkt Repositorien und Materialien" "Unter der Leitung von Constanze Hahn, Jonathan Geiger, Zahra Tabaie, Petra Steiner und Canan Hastik diskutierten Experten aus den DALIA-Partnerkonsortien [NFDI4Chem](https://www.nfdi4chem.de/de/), [NFDI4Ing](https://nfdi4ing.de/), [NFDI4Culture](https://nfdi4culture.de/index.html), [NFDI4Health](https://www.nfdi4health.de/),  [Text+](https://www.text-plus.org/) und der [DINI/Nestor AG Forschungsdaten](https://dini.de/ag/dininestor-ag-forschungsdaten) den Stand von Repositorien und Materialien, Metadaten und Vokabularen. Europaweit ist bereits eine heterogene Landschaft entstanden, die nicht nur aus Open Eduational Resources besteht. Viele Materialien werden auf Plattformen wie YouTube, Zenodo, GitLab, disziplinspezifischen Repositorien, z.B. [Publisso](https://www.publisso.de/open-access-publizieren/repositorien/fachrepositorium-lebenswissenschaften), [Coursera](https://www.coursera.org/), [DARIAHteach](https://teach.dariah.eu/), [CLARIAH tutorial finder](https://teaching.clariah.de/search/), [SSH Open Marketplace](https://www.sshopencloud.eu/ssh-open-marketplace), [Carpentries](https://carpentries.org/index.html), [Free Courses der Harvard University](https://pll.harvard.edu/catalog/free), der [Materialsammlung der UAG-Schulungen der DINInestor AG Forschungsdaten](https://rs.cms.hu-berlin.de/uag_fdm/pages/home.php), sowie als Blogbeiträge oder in Zeitschriften, z.B. [Chemistry Europe](https://chemistry-europe.onlinelibrary.wiley.com/doi/10.1002/cmtd.202200026    ) oder [ing.grid](https://www.inggrid.org/) veröffentlicht. Die Schulungsformate reichen von Moodle-Kursen über Videos, FDM-Grundlagenkurse, Schulungen, Handouts, Präsentationen, Jupyter-Notebooks und Quizze. Kommentare und Bewertungen werden als Feedback-Optionen bevorzugt, um die Relevanz und Qualität des Materials anzuzeigen. Zum Thema Metadaten wurden aktuell verwendete Standards gesammelt und administrative Metadaten für die Materialbereitstellung erörtert." >}}

{{< details "weiter lesen: 6. Juli 2023, online, Schwerpunkt DALIA Suche" "Im dritten Fokusgruppen-Workshop führten Jonathan Geiger, Zahra Tabaie, Jan-Michael Haugwitz, Frank Lange und Canan Hastik in zwei Sessions durch die Themenblöcke Repos, Verantwortlichkeiten und DALIA Suche. In der ersten Sitzung teilten Teilnehmerinnen ihre Erfahrungen und Präferenzen bezüglich der Bereitstellung von OER-Metadaten und diskutierten ihre Erwartungen und zukünftigen Pläne in Bezug auf OER und Repositorien. Dabei wurde betont, dass OER-Repository hohe Benutzerfreundlichkeit und die Möglichkeit disziplinspezifische Materialien zu finden besitzen sollten. Die daran anschließende Diskussion fokussierte Aspekte, welche die Relevanz und Genauigkeit der OER-Materialien gewährleisten; darunter die Mindestaufbewahrungsdauer, die Bedeutung von Kurationsprozessen und regelmäßige Überprüfungsprozesse. In Bezug auf zukünftige Pläne und Erwartungen bezüglich Repositorien teilten TeilnehmerInnen ihre derzeitigen Initiativen und Pläne mit: [BASE4NFDI](https://www.base4nfdi.de/) plant kostenlose Dienste für technische und wissenschaftliche Zielgruppen, [NFDI4memory](https://www.nfdi4memory.de/) strebt an, OER-Materialien in vorhandene Repositorien hochzuladen und im 4Memory-Data-Hub und der DALIA-Suchplattform zu konsolidieren, [FAIRagro](https://www.fairagro.de/) beabsichtigt, fachspezifische OERs zu sammeln und über das FAIRagro-Portal zugänglich zu machen, [NFDI4Chem](https://www.nfdi4chem.de/de/) arbeitet an einem OER4Chem Repo. Alle TeilnehmerInnen bekundeten ihr Interesse, ihre Materialien und/oder Repositorien mit der DALIA-Plattform zu verknüpfen. Im zweiten Teil des Workshops fiel der Blick auf Suchfunktionen und Prozesse. Die Teilnehmerinnen setzen sich mit verschiedenen Suchmechanismen, wie Stichwort-Suche oder Expertensuche auseinander und stimmten über ihre Favoriten als Sucheinstieg ab. Da die Suche nicht immer beim ersten Versuch zum Erfolg führt modellierten und präsentierten die Teilnehmenden ihre individuellen Suchtaktiken. Zum Abschluss wurden farbige Notizzettel, welche Filterkategorien wie Zielgruppe, Thema oder didaktische Aspekte symbolisierten, nach Relevanz sortiert. Mit den gewonnenen Ergebnissen aus diesem Workshop wird nun ein erster Entwurf für die DALIA Benutzerschnittstelle entwickelt." >}}

# DALIA upcoming events

### DALIA Tech Watch   
- 20. September 2023, online, 10:00 - 11:00 Uhr, “LiaScript” mit Sebastian Zug und Andre Dietrich (TU Freiberg)
- 24. Oktober 2023, online, 10:00 - 11:00 Uhr, “Data Literacy Kompetenzprofil für historisch arbeitende Disziplinen” mit Marina Lemaire und Laura Dönig (Uni Trier, eScience)

### SAVE THE DATE | DALIA OER Community Workshop 

- 10. Oktober 2023, online, 9:00 - 12:00 Uhr
- 09. November 2023, online, 9:00 - 12:00 Uhr
- 28. November 2023, online, 9:00 - 12:00 Uhr

# DALIA wird gefördert von

<a href="https://foerderportal.bund.de/foekat/jsp/SucheAction.do?actionMode=view&amp;fkz=16DWWQP07A"><img alt="Logo für Bundesministerium für Bildung und Forschung" src="/newsletter/Foerderhinweis-BMBF.jpg" width="134" height="100" /></a>
<img alt="Logo: &quot;Finanziert von der Europäischen Union - NextGenerationEU" src="/newsletter/Foerderhinweis-EU-vertikal.jpg" width="95" height="100" />

