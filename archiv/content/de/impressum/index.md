---
title: "Impressum"
description: ""
featured_image: 'Dalia_Header_Dark-Blue_RGB_3a_Kopie.jpg'
type: "post"
menu:
  main:
    weight: 1
---

**Herausgeber**

Technische Universität Darmstadt 

Institut für Fluidsystemtechnik

Otto-Berndt-Str. 2

D-64287 Darmstadt

Telefon: +49 6151 16-27100

Telefax: +49 6151 16-27111

dalia@fst.tu-darmstadt.de

**Datenschutzerklärung**

Dieser Newsletter wird im Rahmen des DFN-Mailinglistendienstes gehostet und unterliegt dessen Datenschutzbestimmungen bezüglich personenbezogener Daten. Die Erhebung personenbezogener Daten (gemäß Art. 4 Nr. 1 der EU-Datenschutzgrundverordnung (DSGVO)) erfolgt nur durch die vom Nutzer-Browser übermittelten Daten (Server-Logfiles). Weitere Informationen über die Sammlung von personenbezogenen Daten können Sie auf [der Informationsseite des Steinbuch Centre for Computing (SCC) nachlesen](http://www.scc.kit.edu/dienste/16863.php).

Die Technische Universität Darmstadt stellt die verantwortlichen Mitglieder für diesen Newsletter. Die Universität hält sich an die EU-DSGVO. Wenn Sie mit Mitgliedern der Universität in Verbindung treten, werden Ihre Angaben zur Bearbeitung der Anfrage sowie für den Fall, dass Anschlussfragen entstehen, gespeichert. [Weitere Informationen finden Sie auf der Website der Technischen Universität Darmstadt](http://www.tu-darmstadt.de/datenschutzerklaerung.de.jsp).

Gemäß Art. 6 Sec. 1 cl. 1 lit. a DSGVO nutzen wir Ihre E-Mail-Adresse, um Ihnen regelmäßig unseren Newsletter zuzusenden, sofern Sie sich explizit in einen E-Mail-Verteiler angemeldet haben.

Sie können sich auch jederzeit [von diesem Newsletter abmelden](http://www.listserv.dfn.de/sympa/info/dalia_newsletter).

Dieses Projekt ist gefördert von

<a href="https://foerderportal.bund.de/foekat/jsp/SucheAction.do?actionMode=view&amp;fkz=16DWWQP07A"><img alt="Logo for Bundesministerium für Bildung und Forschung" src="/newsletter/Foerderhinweis-BMBF.jpg" width="134" height="100" /></a>
<img alt="Logo: &quot;Finanziert von der Europäischen Union - NextGenerationEU" src="/newsletter/Foerderhinweis-EU-vertikal.jpg" width="95" height="100" />
