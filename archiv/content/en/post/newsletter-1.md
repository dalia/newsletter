---
date: "2023-07-01"
description: ""
header_image: "/Dalia_Header_Dark-Blue_RGB_3a_Kopie.jpg"
featured_image: "/DALIA2.jpg"
tags: ["newsletter"]
title: "No. 1, July 2023"
---

# DALIA introduction

![DALIA team foto](/newsletter/DALIA2.jpg)

From 24-25 March 2023, the DALIA team met for the kick-off in Darmstadt. Following the paradigm of FAIR data use and data supply, the DALIA vision was jointly developed to promote "data literacy from the very beginning". 

[[More information]](https://www.fst.tu-darmstadt.de/fachgebiet/news_details_522688.de.jsp)

# DALIA network

DALIA stands for Data Literacy Alliance. Through the project partners' ties with the [NFDI e.V.](https://www.nfdi.de/), the sections [Training & Education](https://www.nfdi.de/section-edutrain/), [ELSA - Ethical, Legal & Social Aspects](https://www.nfdi.de/section-elsa/) and [(Meta)daten, Terminologien, Provenienz](https://www.nfdi.de/section-metadata/) ensures that DALIA is sustainably integrated and embedded in the National Research Data Infrastructure Germany (NFDI).

# DALIA technology

### DALIA, what are you?

DALIA is a knowledge base.

In DALIA, the [Research Data Services](https://www.ulb.tu-darmstadt.de/die_bibliothek/ueberuns/organisation/abteilung_iii/forschungsdaten_services.en.jsp) team at the Darmstadt University and State Library (TU Darmstadt) is developing an ontology-based knowledge graph as a basis for the efficient integration and networking of specialised distributed data sources to promote data literacy. The benefits of logical reasoning are used to support the curation of learning materials and EduBricks as course paths and to enrich the recommendation system.

# DALIA Tech Watch Series

All lectures in this series are available on the [DALIA YouTube channel](https://www.youtube.com/channel/UC6f7Bm8AGdfWuAvSAwe2Bjg).

### 19 February 2023, eDoer Prototype Workshop by Gábor Kishmihók
In February 2023, our project partners from TIB Hannover presented eDoer, the prototype of the their open AI-based recommendation system for educational material. In addition related projects eDoer Knowledge Graph and EduCOR ontology are introduced. 

{{< details "read more" "eDoer is a recommendation system for accessible educational content. The platform provides access to a wide range of open educational resources and uses machine-learning algorithms to help both learners and educators find appropriate, personalised educational material. With the use of AI-powered algorithms, users' learning data is analysed in terms of learning preferences and goals to provide content to best meet the individual needs of each learner. For more information on eDoer, please visit the [website](https://www.edoer.eu)." >}}


### 06 June 2023, CESSDA Data Catalogue by Maja Dolinar
In this DALIA Tech Watch-Session, Maja Dolinar (University of Ljubljana) presented the latest version of the CESSDA Data Catalogue (CDC). CESSDA brings together social science data archives from across Europe, aiming to promote the results of social science research and support national research as well as international cooperation. 
 
{{< details "read more" "The Cessda Data Catalogue (CDC) is the platform when it comes to research on social science data in Europe. Research data can be quantitative, qualitative or mixed data, cross-sectional or longitudinal, recently collected or historical. Cessda DC contains descriptions from over 40,000 data collections (from over 20 European countries) managed by the CESSDA service providers (SPs). For more information on the CESSDA CD, please visit the [website](https://www.cessda.eu/Tools/Data-Catalogue)." >}}

### 20 July 2023, NFDICulture Knowledge Graph by Etienne Posthumus
On June 20, 2023, Etienne Posthumus (FIZ Karlsruhe) gave an introduction to Knowledge Graph (KG) based RDM solutions. He presented the [Culture Knowledge Graph](https://nfdi4culture.de/services/details/culture-knowledge-graph.html) and its application. 

{{< details "read more" "The Culture KG creates a link between all research data generated within NFDI4Culture subject areas, aiming to improve the discoverability, accessibility, interoperability and reusability of data within NFDI4Culture. For more information on the Culture KG and NFDI4Culture, please visit the [website](https://nfdi4culture.de/de/dienste/details/culture-knowledge-graph.html)." >}}

# DALIA OER Community Workshops

With this series of workshops, a platform for active exchange and cross-project collaboration is promoted and consolidated. Continuous communication is seen as a key factor for the success of the project, therefore all future user groups should actively participate in the development and the existing communities of practice should be further expanded. The results of the workshop will be published on the website.

{{< details "read more: 6 June 2023, online, focus on teaching and didactics" "During the moderated focus group discussion, experts led by Sonja Herres-Pawlis, Britta Petersen, Constanze Hahn and Petra Steiner discussed requirements and concepts in DALIA regarding teaching, didactics, personas, user roles and metadata. The discussion covered various aspects of the organisation and integration of teaching and learning materials. Participants emphasised the importance of quality, reusability and trust in the DALIA platform, similar to existing offerings such as [Carpentries](https://carpentries.org/index.html), [Elixir Tess](https://tess.elixir-europe.org) and [dariahTeach](https://teach.dariah.eu/). Necessary quality criteria such as professional, legal and didactic aspects were mentioned in order to address users adequately and to offer diverse learning paths for individual scenarios. In this context, user roles of the potential DALIA target groups were discussed with a view to personas, e.g. [Bioschemas TrainingMaterial Profile](https://bioschemas.org/profiles/TrainingMaterial/1.0-RELEASE). There will be another in-depth workshop on this topic. Furthermore, an outlook on the handling of metadata was given. In addition to using existing standards, approaches that have already been adopted in the community, such as the [Lernzielmatrix zum Themenbereich Forschungsdatenmanagement für die Zielgruppen Studierende, PhDs und Data Stewards](https://zenodo.org/record/7034478), will also be considered. Furthermore, the DALIA team decided to maintain a glossary to ensure consistent definitions and a common understanding of terms such as persona, user story, use case, and others." >}}

{{< details "read more: 16 June 2023, online, focus on repositories and materials" "Experts from the DALIA partner consortia, e.g. [NFDI4Chem](https://www.nfdi4chem.de/de/), [NFDI4Ing](https://nfdi4ing.de/), [NFDI4Culture](https://nfdi4culture.de/index.html), [NFDI4Health](https://www.nfdi4health.de/), [Text+](https://www.text-plus.org/), and the [UAG Schulungen/Fortbildungen der DINInestor-AG Forschungsdaten](https://www.forschungsdaten.org/index.php/UAG_Schulungen/Fortbildungen) und Materialsammlung discussed the state of repositories and materials, metadata and vocabularies under the leadership of Constanze Hahn, Jonathan Geiger, Zahra Tabaie, Petra Steiner and Canan Hastik. Across Europe, a heterogeneous landscape has already emerged in recent years, which not only includes Open Eduational Resources. Many materials are published on platforms such as YouTube, Zenodo, GitLab, discipline-specific repositories, e.g. [Publisso](https://www.publisso.de/open-access-publizieren/repositorien/fachrepositorium-lebenswissenschaften), [Coursera](https://www.coursera.org/), [DARIAHteach](https://teach.dariah.eu), [CLARIAH tutorial finder](https://teaching.clariah.de/search/), [SSH Open Marketplace](https://www.sshopencloud.eu/ssh-open-marketplace), [Carpentries](https://carpentries.org/index.html), [Free Courses from Harvard University](https://pll.harvard.edu/catalog/free), the [collection of materials from the UAG training courses of the DINInestor-AG Forschungsdaten](https://rs.cms.hu-berlin.de/uag_fdm/pages/home.php), and as blog posts or in journals, e.g. [Chemistry Europe](https://chemistry-europe.onlinelibrary.wiley.com/doi/10.1002/cmtd.202200026   ) or [ing.grid](https://www.inggrid.org/). Training formats range from Moodle courses to videos, RDM basic courses, training courses, handouts, presentations, Jupyter notebooks, and quizzes. Comments and ratings are preferred as feedback options to indicate relevance and quality of the material. On the topic of metadata, currently used standards were collected and administrative metadata for material providers were discussed." >}}

{{< details "read more: 6 July 2023, online, focus on DALIA search" "This workshop Jonathan Geiger, Zahra Tabaie, Jan-Michael Haugwitz, Frank Lange and Canan Hastik lead two sessions on repos, responsibilities and DALIA search. In the first session, participants shared their experiences and preferences regarding the provision of OER metadata, discussed their expectations and future plans regarding OER repositories. It was emphasised that OER repositories should be user-friendly and have the possibility to find discipline-specific materials. The discussion focused on aspects that ensure the relevance and accuracy of OER materials, including the minimum retention period, the importance of curation and review processes. In terms of future plans and expectations regarding repositories, participants shared their current initiatives and plans: [BASE4NFDI](https://www.base4nfdi.de/) plans free services for technical and scientific audiences, [NFDI4memory](https://www.nfdi4memory.de/) aims to upload OER materials to existing repositories and consolidate them in the 4Memory data hub and the DALIA search platform, [FAIRagro](https://www.fairagro.de/) intends to collect subject-specific OERs and make them accessible via the FAIRagro portal, [NFDI4Chem](https://www.nfdi4chem.de/de/) is working on an OER4Chem repo. All participants expressed interest in linking their materials and/or repositories to the DALIA platform. The second part of the workshop focused on search functions and processes. The participants discussed different search mechanisms, such as keyword search or expert search, and voted on their favourites. As the search does not always lead to success at the first attempt, the searchers modelled and presented their individual search tactics. Finally participants sorted coloured sticky notes symbolising filter categories such as target group, topic or didactic aspects by relevance. With the results obtained from this workshop, the DALIA user interface is now being developed." >}}

# DALIA Upcoming Events

### DALIA Tech Watch  

- 20 September 2023, online, 10.00 a.m. - 11.00 a.m., “LiaScript” with Sebastian Zug and Andre Dietrich (TU Freiberg)
- 24 October 2023, online, 10.00 a.m. - 11.00 a.m. “Data Literacy Kompetenzprofil für historisch arbeitende Disziplinen” with Marina Lemaire and Laura Dönig (Universität Trier, eScience)

### SAVE THE DATE | DALIA OER Community Workshop 

- 10 October 2023, online, 9.00 a.m. - 12.00 p.m.
- 09 November 2023, online, 9.00 a.m. - 12.00 p.m.
- 28 November 2023, online, 9.00 a.m. - 12.00 p.m.

# DALIA is funded by

<a href="https://foerderportal.bund.de/foekat/jsp/SucheAction.do?actionMode=view&amp;fkz=16DWWQP07A"><img alt="Logo for Bundesministerium für Bildung und Forschung" src="/newsletter/Foerderhinweis-BMBF.jpg" width="134" height="100" /></a>
<img alt="Logo: &quot;Finanziert von der Europäischen Union - NextGenerationEU" src="/newsletter/Foerderhinweis-EU-vertikal.jpg" width="95" height="100" />

