---
title: "Article"

description: "Data Literacy Alliance Knowledge-base for FAIR data usage and supply"
cascade:
featured_image: 'bg_dark_3.jpg'

---

With the motto "Data literacy from the beginning!", DALIA launches the development of a central entry point for a federated knowledge base for teaching and learning materials to promote data literacy in the field of FAIRe data use and supply.
