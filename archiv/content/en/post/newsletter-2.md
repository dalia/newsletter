---
date: "2024-03-02"
description: ""
header_image: "/Dalia_Header_Dark-Blue_RGB_3a_Kopie.jpg"
featured_image: "/Gruppenfoto2.jpg"
tags: ["newsletter"]
title: "No. 2, March 2024"
---

# DALIA after one year

![Gruppenfoto](/newsletter/Gruppenfoto_3.jpg)

DALIA stands for [Data Literacy Alliance](https://dalia.education/). After the first year of the project, DALIA met again at the Technical University of Darmstadt, this time organized by the team from the University and State Library. The DALIA team discusses the results of the first year of the project and determines the next steps.

At the next NFDI section members meeting on May 16, 2024 from 1:00 p.m. to 3:00 p.m., DALIA plans to present the DALIA prototype in a first version together with a recommendation for the NFDI community for providing, combining and making visible teaching and Learning materials in the portal. In this context, DALIA is announcing a call to fill the portal with test data (teaching and learning materials).

The connection of the project partners with the [NFDI e.V.](https://www.nfdi.de/) and in particular the [Sektion Training & Education](https://www.nfdi.de/section-edutrain/) ensures that DALIA is sustainably integrated and embedded into the National Research Data Infrastructure (NFDI) community. Planning further ahead and to secure the DALIA platform sustainably, a [Base4NFDI](https://base4nfdi.de/) application is being prepared together with the [Sektion Training & Education](https://www.nfdi.de/section-edutrain/). 

# DALIA and the NFDI Section EduTrain

The activities of the [NFDI Sektion Training & Education](https://www.nfdi.de/section-edutrain/) which are closely linked to the DALIA project, will now have their own place in the DALIA newsletter so that they can also be communicated within this framework. To start with, we will briefly outline what is currently happening in the individual work packages: 
White papers are being written in work packages 1 (target group and needs analysis), 3 (development of a modular and scalable concept) and 8 (error culture). The white paper on [EduBricks: A concept for modular and scalable teaching material](https://docs.google.com/document/d/1XpHsC6HmEXGLpGOrdwu-szg5WTkb8-Q9NQ47cr14dhY/edit#heading=h.u6ni3dwuzt3h) is already available for comment by the EduTrain community. Furthermore, an editorial team is working hard to revise the [learning objective matrix on research data management](https://zenodo.org/records/7034478). Current information from the EduTrain section, as well as all other events, is continuously communicated via various NFDI channels, such as the [Section mailinglist](https://lists.nfdi.de/postorius/lists/section-edutrain.lists.nfdi.de/) and NFDI section [RocketChat Channels](https://all-chat.nfdi.de/channel/Section-TrainingEducation).

### Upcoming Events:

- Every first Monday of the month from 11:30 a.m. to 12:30 p.m., AP 2 - Inventory of teaching and learning materials of the NFDI Section EduTrain meets online in this [Zoom room](https://adwmainz.zoom.us/j/92476322440).
- On March 20, 2024 from 2:00 p.m. to 3:00 p.m. the training formats and certificate course of the NFDI section EduTrain AP 5 will meet, online in this [Zoom room](https://us02web.zoom.us/j/89267516658?pwd=M3Fvai83KzRMZzE1VHBYN01SVXRJdz09).
- The next NFDI Section EduTrain general meeting will take place on May 16, 2024 from 1 p.m. to 3 p.m.
- On June 18, 2024 from 11 a.m. to 1 p.m., WP 1 - Target Group and Needs Analysis of the NFDI Section EduTrain will organize another workshop on the development of personas in WP 1/DALIA "Shaping EduTrain/Dalia Personas II". This workshop will focus on specifying the needs of (material re-using) lecturers and content/material contributors in DALIA. The results of the workshop will be used to sharpen the already outlined descriptions of the personas.


# Joint Event on the Learning Objectives Matrix for Research Data Management

![Gruppenfoto](/newsletter/Gruppenfoto2.jpg)

From January 31st to February 1st, 2024, 57 interested members of the research data management community, including those from the NFDI, the data competence centers and the state initiatives, met in the Georg-Christoph-Lichtenberg-Haus in Darmstadt. At the community event organized by the [Data Literacy Alliance](https://dalia.education), the  [NFDI Sektion Training und Education](https://www.nfdi.de/section-edutrain/), and the [UAG Schulungen/Fortbildungen der DINI/nestor-AG Forschungsdaten](https://www.forschungsdaten.org/index.php/UAG_Schulungen/Fortbildungen) the current status of the [learning goal matrix for research data management](https://zenodo.org/doi/10.5281/zenodo.7034477 ) was discussed, revised and further developed in order to use it in the future to establish the basis for promoting competence in the area of ​​research data management.
In the next few weeks, the community can look forward not only to a new version of the learning goal matrix, but also to a linked open data model of the same.


# DALIA Tech Watch 

The lectures in this series are available on the [DALIA YouTube channel](https://www.youtube.com/channel/UC6f7Bm8AGdfWuAvSAwe2Bjg).

### September 20, 2023, "LiaScript" with Sebastian Zug and André Dietrich (TU Freiberg)
On September 20, 2023, Sebastian Zug and André Dietrich presented concepts,  implementations and experiences from many years of using LiaScript.


{{< details "read more" "Sebastian Zug and André Dietrich presented LiaScript, a Markdown-based tool for creating interactive, open and interoperable educational content. By extending Markdown with functions such as multimedia, quizzes, animations and interactive programming, a wide range of learning materials can be created even without extensive programming knowledge. These can be exported in various formats and imported into different learning management systems. In addition, the content created can be shared as Markdown documents in Git repositories to ensure its readability and editability and to enable easy reuse. Instructions for LiaScript are available on its own [YouTube Channel](https://www.youtube.com/channel/UCyiTe2GkW_u05HSdvUblGYg)." >}}


### October 24, 2023, "Data literacy competence profile for disciplines working in history" with Marina Lemaire and Laura Dönig (University of Trier, eScience) 
On October 24, Laura Dönig and Marina Lemaire gave insights into working and mapping data literacy matrices with the FuD virtual research environment for humanities and social sciences.

{{< details "read more" "As part of the NFDI4Memory consortium, Task Area 4 Data Literacy is working on the development of services, handouts and innovative teaching and learning formats to improve data literacy (DL) and active research data management (RDM) in the disciplines working in history. The development of a historically-oriented DL competence profile is the basis for a DL model curriculum as well as epoch- and area-specific curricula. It will also be used to classify the courses in the DL training catalog. The work began with the planning and implementation of the data literacy needs assessment, for which a questionnaire design had to be developed. For this purpose, a mapping of competence matrices was carried out with the aid of the [Forschungssoftware FuD](https://fud.uni-trier.de/) which allows the representation of the whole research process, from inventorisation of primary data through analysis and publication until archiving. In several mapping iterations, the individual competence profiles were mapped to each other, so that after each mapping phase an extended profile was created, which in turn became the starting point for the subsequent mapping. The aim of the comparison was to generate a merged, specialized competency matrix for the historical sciences from the individual DL competency profiles." >}}


# DALIA OER Community Workshops

This series of workshops will promote and consolidate a platform for active exchange and cross-project collaboration. Continuous communication is seen as a key factor for the success of the project, which is why all future user groups should actively participate in the development and the existing communities of practice should be further expanded. The workshop results will be published on the website at a later date.

{{< details "read more: 10. Oktober 2023, online, focus on EduTrain Personas" "The various consortia within the NFDI have made impressive progress in the area of Training & Education (EduTrain), either formulating personas or identifying discipline-specific use cases to meet the needs of their target groups. DALIA pursued this approach together with Britta Petersen lead of AP1 of the EduTrain section and took it up during a side event at [CoRDI 2023](https://www.nfdi.de/cordi-2023/) and further expanded it during the community event on October 10, 2023. Looking at the NFDI shows that the consortia address the needs of their target groups with three basic persona concepts: content provider, trainer and learner. These are now being adopted and further characterized by DALIA. For each of these target groups, contact persons will be available in the section Training and Education in the future to further refine the specific needs and transfer them to the DALIA project. We look forward to following up on these developments together with the existing variety of training courses within the NFDI and offering them in the DALIA portal. A detailed report on the workshop is currently being prepared as a white paper." >}}

{{< details "read more: 09 November 2023, online, focus on metadata harmonization" "The harmonization of metadata is an important part of the DALIA project for the scientific community. This involves the standardization of terms and their definitions as well as the linking of metadata classes from different standards. In this community workshop, scientists from DALIA, NFDI consortia, the DINI AG KIM, the SODa (Center for Collections, Objects, Data Competencies), the Trier Center of Digital Humanities and other institutions came together to discuss the current status. Various presentations formed the introduction to the topic area: Petra Steiner (DALIA) gave an introduction to DALIA's minimal metadata sets and their description in the form of Tabular Application Profiles (TAPs), which are not only an efficient and transparent means of describing metadata standards, but also serve to formally validate RDF graphs. Katharina Bergmann and Andrea Polywka from [NFDI4Culture](https://nfdi4culture.de/) presented the project’s terminology and [Educational Resource Finder](https://nfdi4culture.de/services/details/educational-resource-finder.html) Susanne Arndt from the [NFDI WG Cookbooks, Guidance, and Best Practices](https://zenodo.org/records/6758256) gave an overview of the WG's metadata categories and material collections. Mark Fichtner presented the three ways in which the Data Competence Centre plans to use application-oriented teaching methods to promote interdisciplinary skills: onboarding, analogue and digital. The training materials on metadata and terminology have a large overlap with the subject area of DALIA. After compiling this general information, three parallel groups discussed their joint ideas on minimal metadata sets for teaching and learning resources. This resulted in numerous metadata categories, but also definitional issues. This workshop initiated several further meetings and had a major impact on the development of DALIA's minimal metadata set." >}}

{{< details "read more: November 28, 2023, online, focus on DALIA Frontend" "In the last workshop of the year, the DALIA team presented the initial results of the DALIA portal development to the FDM community and discussed the front end of the education platform under development with participants. On November 28, 2023, the DALIA project team invited participants to jointly test and evaluate the interface of the DALIA portal in a three-hour workshop. In close collaboration with the design agency [Blue World Studios](https://www.blueworld.studio/) the project group had spent the previous two months developing the homepage, including the keyword search function, the results view for resources and the profile and community pages, which now required feedback from the community. In joint discussions on the individual frames of the resulting website, new ideas and perspectives were introduced that gave the team a deeper understanding of how the functions and layout of the interface can be optimized for future users. The feedback was then carefully evaluated by the team and incorporated into further development. We would like to thank all the participants who took part in the workshop and whose enthusiasm and ideas are driving the completion of the platform and making it a better place for teaching and learning. We look forward to sharing the results of our work with the community this year." >}}

# DALIA upcoming events

### DALIA Tech Watch   
- 13. März 2024, online, 10:00 - 11:00 Uhr, [Open Research Knowledge Graph (ORKG)](https://www.youtube.com/watch?v=gDNt3C57OO0) mit Vinodh Ilangovan
- May 7, 2024 , online, 13:00 - 14:00, "Of data, skills and the data literacy toolbox - an overview" with Christian Zinke-Wehlmann (InfAI)
- June 12, 2024, online, 9:30 - 10:30 a.m., "Navigating Open Horizons: Strategies for Fostering OER competences and activities in Austrian higher education" with Sandra Schön (TU Graz)
- July 3, 2024, online, 13:00 - 14:00, "Educational Resource Finder (ERF)" with Andrea Polywka, Katharina Bergmann and Linnaea Söhn (NFDI4Culture)


### SAVE THE DATE | Other Events and Workshops

- May 16, 2024, online, NFDI section EduTrain general meeting
- October 21-23, 2024, Berlin, [EOSC Symposium](https://eosc.eu/events/eosc-symposium-2024/)


# DALIA is funded by

<a href="https://foerderportal.bund.de/foekat/jsp/SucheAction.do?actionMode=view&amp;fkz=16DWWQP07A"><img alt="Logo für Bundesministerium für Bildung und Forschung" src="/newsletter/Foerderhinweis-BMBF.jpg" width="134" height="100" /></a>
<img alt="Logo: &quot;Finanziert von der Europäischen Union - NextGenerationEU" src="/newsletter/Foerderhinweis-EU-vertikal.jpg" width="95" height="100" />


