---
title: "Publisher"
description: ""
featured_image: 'Dalia_Header_Dark-Blue_RGB_3a_Kopie.jpg'
type: "post"
menu:
  main:
    weight: 1
---

**Publisher**

Technical University Darmstadt

Chair of Fluid Systems

Otto-Berndt-Str. 2

D-64287 Darmstadt

Telefon: +49 6151 16-27100

Telefax: +49 6151 16-27111

dalia@fst.tu-darmstadt.de

**General Data Protection Regulation**

This newsletter is hosted as part of the DFN mailing list service and is subject to its data protection provisions regarding personal data. The collection of personal data (according to Art. 4 No. 1 of the EU General Data Protection Regulation (GDPR)) only takes place through the data transmitted by the user's browser (server log files). Further information on the collection of personal data can be found on [the information page of the Steinbuch Centre for Computing (SCC)](http://www.scc.kit.edu/dienste/16863.php).

The Technical University Darmstadt provides the responsible members for this newsletter. The university complies with the EU General Data Protection Regulation. If you contact members of the university, your details will be stored for the purpose of processing your enquiry and in case follow-up questions arise. [Further information can be found on the website of the Technical University Darmstadt](http://www.tu-darmstadt.de/datenschutzerklaerung.de.jsp).

Pursuant to Art. 6 Sec. 1 cl. 1 lit. a GDPR, we use your email address to send you our newsletter on a regular basis, provided you have explicitly subscribed to an email distribution list.

You can also [unsubscribe from this newsletter](htpp://www.listserv.dfn.de/sympa/info/dalia_newsletter) at any time.

This project is funded by 

<a href="https://foerderportal.bund.de/foekat/jsp/SucheAction.do?actionMode=view&amp;fkz=16DWWQP07A"><img alt="Logo for Bundesministerium für Bildung und Forschung" src="/newsletter/Foerderhinweis-BMBF.jpg" width="134" height="100" /></a>
<img alt="Logo: &quot;Finanziert von der Europäischen Union - NextGenerationEU" src="/newsletter/Foerderhinweis-EU-vertikal.jpg" width="95" height="100" />
