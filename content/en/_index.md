---
title: "DALIA Newsletter"

description: "Data Literacy Alliance Knowledge-base for FAIR data usage and supply"
cascade:
  featured_image: 'bg_dark_3.jpg'
---

Our Newsletter location has changed. You can find it integrated in the [DALIA project website](https://dalia.education/en/newsletter).

Thank you for your interest and support