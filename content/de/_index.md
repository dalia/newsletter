---
title: "DALIA Newsletter"

description: "Data Literacy Alliance Knowledge-base for FAIR data usage and supply"
cascade:
  featured_image: 'bg_dark_3.jpg'
---

Unser Newsletter ist umgezogen auf die DALIA Projektwebseite [zum neuen Newsletter](https://dalia.education/de/newsletter).

Vielen Dank für ihr Interesse